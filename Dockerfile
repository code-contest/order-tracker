FROM node:12.4.0-alpine

WORKDIR /app
ADD ./source /app/source
ADD package.json .
ADD tsconfig.json .

RUN npm install

CMD [ "npm", "start" ]