import * as Express from "express"
import * as BodyParser from "body-parser"
import * as Logger from "morgan"

import * as OrderController from "./OrderController"

const app = Express()

// The middleware chain
//
// 1. Logger
// 2. HTTP request body parser (for JSON content)
// 3. Request handlers
// 4. Error response handler 

// Use standard Apache common log output in production
const logFormat = process.env.NODE_ENV === "production" ? "common" : "dev"
app.use(Logger(logFormat))

app.use(BodyParser.json())

app.get("/health", (_, response) => {
    response.status(200).json({
        service: "order-service",
        status: "OK"
    })
})

app.get("/", OrderController.GetAll)
app.post("/", OrderController.Create)
app.get("/:orderID", OrderController.GetById)

app.use((error: Error, _: Express.Request, response: Express.Response, __: Express.NextFunction) => {
    response.status(500).json(error.message)
})

const port = process.env.TARGET_PORT
app.listen(port, () => console.info(`order-service listening at ${port} ...`))