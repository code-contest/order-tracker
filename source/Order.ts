import UUID from "./UUID"

interface Postage {
    date?: string,
    address: string
}

export default class Order {
    public _id: string
    public status: string
    public pickup: Postage
    public dropoff: Postage

    public constructor(order: any) {
        this._id = order._id || UUID()
        this.status = order.status || "Pending"
        
        if (!this.isValid(order.pickup) || !this.isValid(order.dropoff)){
            throw "Please check the pickup/dropoff date & address and try again."
        }

        this.pickup = order.pickup
        this.dropoff = order.dropoff
    }

    private isValid(postage: Postage): boolean {
        if (postage && postage.address) {
            if (postage.date) {
                return Date.parse(postage.date) !== NaN
            }

            return true
        }
        
        return false
    }
}