/**
 *  Returns a version 4 UUID as per RFC 4122.
 *  The version 4 UUID is meant for generating UUIDs from truly-random or pseudo-random numbers. 
 *  
 *  @export
 */
export default function UUID(): string {
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, (char) => {
        // Get a pseudo-random number between 0 and 16
        // The bitwise | (or) operator converts its operands to a 32-bit signed integer,
        // which means the fractional part gets dropped. We could have used Math.trunc() 
        // instead but bitwise OR has been found to be more efficient.
        let random = Math.random() * 16 | 0

        // Replace x's with a random number between 0 and 15 (0x0 - 0xf)
        // Replace y with a random number between 8 and 11 (0x8 - 0xb)
        let value = char === "x" ? random : (random % 4 + 8)

        // Return hexadecimal representation
        return value.toString(16)
    });
}