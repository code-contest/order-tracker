import { MongoClient } from "mongodb"
import { Request, Response, NextFunction } from "express"
import Order from "./Order"

const dbURI = process.env.DB_CONN_STRING

export async function GetAll(_: Request, response: Response, next: NextFunction) {
    try {
        const dbClient = await MongoClient.connect(dbURI!, { useNewUrlParser: true })
        const ordersCollection = dbClient.db("order-service").collection("orders")
        const docs = await ordersCollection.find({})

        const orders = Array<Order>()
        await docs.forEach(doc => {
            orders.push(new Order(doc))
        })

        dbClient.close()
        response.status(200).json(orders)
    }
    catch(error) {
        next(error)
    }
}

export async function GetById(request: Request, response: Response, next: NextFunction) {
    try {
        const dbClient = await MongoClient.connect(dbURI!, { useNewUrlParser: true })
        const ordersCollection = dbClient.db("order-service").collection("orders")

        const order = await ordersCollection.findOne({ _id: request.params.orderID })
        dbClient.close()

        if (order) {
            response.status(200).json(new Order(order))
        }
        else {
            throw new Error("Order ID not found.")
        }
    }
    catch(error) {
        next(error)
    }
}

export async function Create(request: Request, response: Response, next: NextFunction) {
    try {
        const dbClient = await MongoClient.connect(dbURI!, { useNewUrlParser: true })
        const ordersCollection = dbClient.db("order-service").collection("orders")

        const order = new Order(request.body)
        const result = await ordersCollection.insertOne(order)
        dbClient.close()

        response.status(200).json(result.ops.pop())
    }
    catch(error) {
        next(error)
    }
}