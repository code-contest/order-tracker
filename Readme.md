# order-service

Creates and retrieves an `order`.

## The `order` type
```
Order {
  _id: UUID,
  status: String,
  pickup: {
    date: String,
    address: String
  },
  dropoff: {
    date: String,       // Set upon delivery
    address: String
  }
}
```

## Unit tests
```
$ npm test
```

## Run the application
```
$ docker-compose up
```

Routes supported:
```
GET /             // all orders
GET /<orderID>    // order with given ID
POST /            // new order; must send valid pickup and dropoff details
```

## Design choices

#### UUIDs
I chose to generate RFC 4122 version 4 UUIDs myself. Unlike time-based and name-based UUIDs, version 4 
UUIDs are created from pseudo-random numbers entirely and thus quite straightforward to generate. If we do
switch to other UUIDs, I'd rather use a third-party UUID library for maximally efficient generation.

#### Data persistence
I have used MongoDB database for data storage simply because I didn't want to have any schema-related overhead.
My typical workflow is to always start with schemaless databases to allow for rapid prototyping. Once the dust
settles, we reach a point where the data modelling requirements are well understood. 

Then, if the data is found to be relational, (and orders do tend to be relational), it is typically worth swapping 
the NoSQL storage with a SQL database such as PostgreSQL.
If the data is not relational, I usually replace my model controllers with an ORM like Mongoose.

#### Automated testing
I have included unit tests for:
  - creating order objects from HTTP request
  - creating order objects from database response
  - generating UUIDs

I didn't use mocks to simulate database responses because it would be overkill for this small and straightforward service.

I did not include any automated integration testing because:
  - there is only one external service, Mongodb
  - we are connecting to it using the official driver which is thoroughly tested

#### Docker
I have used Docker to put together the entire application. The `docker-compose up` command fires up:
  - the `database` container, running a MongoDB instance
  - the `order-service` container, running the actual app

Both these containers are connected to their own Docker network called `order-network`. Thus avoiding hostname/port 
collisions and other network issues.

The `database` container persists the MongoDB data to a Docker volume `order-db-data`. This ensures that MongoDB
data (at `/data/db` within container) is not lost when the container is stopped. Also enables multiple containers
to have shared access to order data.

#### The .env file
I decided to commit `.env` file to source control so that you can test this easily. In practice, I keep environment
variables out of source control as they often contain sensitive information.

I do not read the `.env` file directly in my code. Rather, `docker-compose.yml` parses it to setup the environment.
`docker-compose.yml` first looks for the variables in the shell context, and if not found there then in an `.env` file. 
In production, the `.env` is replaced by setting environment variables on the virtual machine.
For CI/CD, we can set environment variables in the pipeline configuration settings on the web portal.

There is also the option of using a secrets management services (a key-value store accessible through REST) but I
haven't worked with them yet.