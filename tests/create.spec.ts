import * as Chai    from "chai"
import Order        from "../source/Order"

// Chai plugin for performing assertion on UUIDs
Chai.use(require("chai-uuid"))
const expect = Chai.expect

describe("◊◊◊  Create an order  ◊◊◊", () => {
    const requestBody = {
        pickup: {
            date: "2019-06-11T10:10:00.000Z", 
            address: "Aldgate East"
        },
        dropoff: {
            address: "Farringdon"
        }
    }

    it("Should instantiate the Order from request body properly", () => {
        expect(new Order(requestBody)).to.not.throw

        const order = new Order(requestBody)
        expect(order).to.not.be.null
        expect(order._id).to.be.uuid()
        expect(order.status).to.equal("Pending")

        expect(order.pickup).to.deep.equal({
            date: "2019-06-11T10:10:00.000Z", 
            address: "Aldgate East",
        })
        expect(order.dropoff).to.deep.equal({
            address: "Farringdon"
        })
    })
})