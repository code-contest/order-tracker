import * as Chai from "chai"
import UUID from "../source/UUID"

// Chai plugin for performing assertion on UUIDs
Chai.use(require("chai-uuid"))
const expect = Chai.expect

describe("◊◊◊  UUID  ◊◊◊", () => {
    it("Should generate a valid UUID", () => {
        expect(UUID()).to.be.uuid()
    })
})