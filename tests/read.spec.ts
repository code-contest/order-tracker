import * as Chai from "chai"
import Order from "../source/Order"

// Chai plugin for performing assertion on UUIDs
Chai.use(require("chai-uuid"))
const expect = Chai.expect

describe("◊◊◊  Retrieve an order  ◊◊◊", () => {
    const dbResponse = {
        _id: "48a698a0-1641-5aca-bc1b-de9b1a482ee1",
        status: "Delivered",
        pickup: {
            date: "2019-06-11T10:10:00.000Z", 
            address: "Aldgate East"
        },
        dropoff: {
            date: "2019-06-11T10:10:00.000Z",
            address: "Farringdon"
        }
    }

    it("Should instantiate the Order from database response properly", () => {
        expect(new Order(dbResponse)).to.not.throw

        const order = new Order(dbResponse)
        expect(order).to.not.be.null
        expect(order.status).to.equal("Delivered")
        expect(order._id).to.be.uuid()
        expect(order._id).to.equal("48a698a0-1641-5aca-bc1b-de9b1a482ee1")
        
        expect(order.pickup).to.deep.equal({
            date: "2019-06-11T10:10:00.000Z", 
            address: "Aldgate East"
        })
        expect(order.dropoff).to.deep.equal({
            date: "2019-06-11T10:10:00.000Z",
            address: "Farringdon"
        })
    })
})